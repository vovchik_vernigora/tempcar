﻿using UnityEngine;
using System.Collections;

public class MoveButtonLogic : MonoBehaviour {

    public enum MoveDirection
    {
        Forward, Back
    }

    public MoveDirection move;
    bool pressed;

    void OnPress(bool isPressed)
    {
        pressed = isPressed;
    }

    float deltaMove = 0.07f;
    float lastTimeMove = 0;
    void Update() 
    {
        if (pressed) 
        {
            lastTimeMove += Time.deltaTime;
            if(lastTimeMove > deltaMove)
            {
                lastTimeMove = 0;
                Player.Instance.DoMove(move);
            }
        }
    }
}
