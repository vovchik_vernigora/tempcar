﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public static Player Instance;
    public static GameObject GO;
    public AudioClip impact;

    void Awake() 
    {
        Instance = this;
        GO = Instance.gameObject;
    }

	void Start () {
	
	}
	
	void Update () {
	
	}

    public void DoMove( MoveButtonLogic.MoveDirection direction) 
    {
        switch(direction)
        {
            case MoveButtonLogic.MoveDirection.Forward :
                GO.rigidbody.AddForce(Vector3.right * 100, ForceMode.Force);
                if (GO.rigidbody.velocity.x > 50) { GO.rigidbody.velocity = new Vector3(50, 0, 0); }
                break;
            case MoveButtonLogic.MoveDirection.Back :
                GO.rigidbody.AddForce(Vector3.left * 100, ForceMode.Force);
                if (GO.rigidbody.velocity.x < -50) { GO.rigidbody.velocity = new Vector3(-50, 0, 0); }
                break;
            default: break;
        }
    }

    void OnGUI() 
    {
        GUI.Label(new Rect(0, 0, 200, 50), "velosity " + GO.rigidbody.velocity);
    }

    void OnTriggerEnter (Collider other) {
        GO.rigidbody.velocity = Vector3.zero;
        audio.PlayOneShot(impact, 0.7F);
		Destroy(other.gameObject);
	}
}
